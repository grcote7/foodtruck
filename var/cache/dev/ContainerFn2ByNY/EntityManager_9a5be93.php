<?php

namespace ContainerFn2ByNY;
include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/lib/Doctrine/Persistence/ObjectManager.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{

    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolderf1763 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer57872 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties9826d = [
        
    ];

    public function getConnection()
    {
        $this->initializer57872 && ($this->initializer57872->__invoke($valueHolderf1763, $this, 'getConnection', array(), $this->initializer57872) || 1) && $this->valueHolderf1763 = $valueHolderf1763;

        return $this->valueHolderf1763->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializer57872 && ($this->initializer57872->__invoke($valueHolderf1763, $this, 'getMetadataFactory', array(), $this->initializer57872) || 1) && $this->valueHolderf1763 = $valueHolderf1763;

        return $this->valueHolderf1763->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializer57872 && ($this->initializer57872->__invoke($valueHolderf1763, $this, 'getExpressionBuilder', array(), $this->initializer57872) || 1) && $this->valueHolderf1763 = $valueHolderf1763;

        return $this->valueHolderf1763->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializer57872 && ($this->initializer57872->__invoke($valueHolderf1763, $this, 'beginTransaction', array(), $this->initializer57872) || 1) && $this->valueHolderf1763 = $valueHolderf1763;

        return $this->valueHolderf1763->beginTransaction();
    }

    public function getCache()
    {
        $this->initializer57872 && ($this->initializer57872->__invoke($valueHolderf1763, $this, 'getCache', array(), $this->initializer57872) || 1) && $this->valueHolderf1763 = $valueHolderf1763;

        return $this->valueHolderf1763->getCache();
    }

    public function transactional($func)
    {
        $this->initializer57872 && ($this->initializer57872->__invoke($valueHolderf1763, $this, 'transactional', array('func' => $func), $this->initializer57872) || 1) && $this->valueHolderf1763 = $valueHolderf1763;

        return $this->valueHolderf1763->transactional($func);
    }

    public function commit()
    {
        $this->initializer57872 && ($this->initializer57872->__invoke($valueHolderf1763, $this, 'commit', array(), $this->initializer57872) || 1) && $this->valueHolderf1763 = $valueHolderf1763;

        return $this->valueHolderf1763->commit();
    }

    public function rollback()
    {
        $this->initializer57872 && ($this->initializer57872->__invoke($valueHolderf1763, $this, 'rollback', array(), $this->initializer57872) || 1) && $this->valueHolderf1763 = $valueHolderf1763;

        return $this->valueHolderf1763->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializer57872 && ($this->initializer57872->__invoke($valueHolderf1763, $this, 'getClassMetadata', array('className' => $className), $this->initializer57872) || 1) && $this->valueHolderf1763 = $valueHolderf1763;

        return $this->valueHolderf1763->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializer57872 && ($this->initializer57872->__invoke($valueHolderf1763, $this, 'createQuery', array('dql' => $dql), $this->initializer57872) || 1) && $this->valueHolderf1763 = $valueHolderf1763;

        return $this->valueHolderf1763->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializer57872 && ($this->initializer57872->__invoke($valueHolderf1763, $this, 'createNamedQuery', array('name' => $name), $this->initializer57872) || 1) && $this->valueHolderf1763 = $valueHolderf1763;

        return $this->valueHolderf1763->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer57872 && ($this->initializer57872->__invoke($valueHolderf1763, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer57872) || 1) && $this->valueHolderf1763 = $valueHolderf1763;

        return $this->valueHolderf1763->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializer57872 && ($this->initializer57872->__invoke($valueHolderf1763, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer57872) || 1) && $this->valueHolderf1763 = $valueHolderf1763;

        return $this->valueHolderf1763->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializer57872 && ($this->initializer57872->__invoke($valueHolderf1763, $this, 'createQueryBuilder', array(), $this->initializer57872) || 1) && $this->valueHolderf1763 = $valueHolderf1763;

        return $this->valueHolderf1763->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializer57872 && ($this->initializer57872->__invoke($valueHolderf1763, $this, 'flush', array('entity' => $entity), $this->initializer57872) || 1) && $this->valueHolderf1763 = $valueHolderf1763;

        return $this->valueHolderf1763->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer57872 && ($this->initializer57872->__invoke($valueHolderf1763, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer57872) || 1) && $this->valueHolderf1763 = $valueHolderf1763;

        return $this->valueHolderf1763->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializer57872 && ($this->initializer57872->__invoke($valueHolderf1763, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer57872) || 1) && $this->valueHolderf1763 = $valueHolderf1763;

        return $this->valueHolderf1763->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer57872 && ($this->initializer57872->__invoke($valueHolderf1763, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer57872) || 1) && $this->valueHolderf1763 = $valueHolderf1763;

        return $this->valueHolderf1763->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializer57872 && ($this->initializer57872->__invoke($valueHolderf1763, $this, 'clear', array('entityName' => $entityName), $this->initializer57872) || 1) && $this->valueHolderf1763 = $valueHolderf1763;

        return $this->valueHolderf1763->clear($entityName);
    }

    public function close()
    {
        $this->initializer57872 && ($this->initializer57872->__invoke($valueHolderf1763, $this, 'close', array(), $this->initializer57872) || 1) && $this->valueHolderf1763 = $valueHolderf1763;

        return $this->valueHolderf1763->close();
    }

    public function persist($entity)
    {
        $this->initializer57872 && ($this->initializer57872->__invoke($valueHolderf1763, $this, 'persist', array('entity' => $entity), $this->initializer57872) || 1) && $this->valueHolderf1763 = $valueHolderf1763;

        return $this->valueHolderf1763->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializer57872 && ($this->initializer57872->__invoke($valueHolderf1763, $this, 'remove', array('entity' => $entity), $this->initializer57872) || 1) && $this->valueHolderf1763 = $valueHolderf1763;

        return $this->valueHolderf1763->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializer57872 && ($this->initializer57872->__invoke($valueHolderf1763, $this, 'refresh', array('entity' => $entity), $this->initializer57872) || 1) && $this->valueHolderf1763 = $valueHolderf1763;

        return $this->valueHolderf1763->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializer57872 && ($this->initializer57872->__invoke($valueHolderf1763, $this, 'detach', array('entity' => $entity), $this->initializer57872) || 1) && $this->valueHolderf1763 = $valueHolderf1763;

        return $this->valueHolderf1763->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializer57872 && ($this->initializer57872->__invoke($valueHolderf1763, $this, 'merge', array('entity' => $entity), $this->initializer57872) || 1) && $this->valueHolderf1763 = $valueHolderf1763;

        return $this->valueHolderf1763->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializer57872 && ($this->initializer57872->__invoke($valueHolderf1763, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer57872) || 1) && $this->valueHolderf1763 = $valueHolderf1763;

        return $this->valueHolderf1763->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer57872 && ($this->initializer57872->__invoke($valueHolderf1763, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer57872) || 1) && $this->valueHolderf1763 = $valueHolderf1763;

        return $this->valueHolderf1763->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializer57872 && ($this->initializer57872->__invoke($valueHolderf1763, $this, 'getRepository', array('entityName' => $entityName), $this->initializer57872) || 1) && $this->valueHolderf1763 = $valueHolderf1763;

        return $this->valueHolderf1763->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializer57872 && ($this->initializer57872->__invoke($valueHolderf1763, $this, 'contains', array('entity' => $entity), $this->initializer57872) || 1) && $this->valueHolderf1763 = $valueHolderf1763;

        return $this->valueHolderf1763->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializer57872 && ($this->initializer57872->__invoke($valueHolderf1763, $this, 'getEventManager', array(), $this->initializer57872) || 1) && $this->valueHolderf1763 = $valueHolderf1763;

        return $this->valueHolderf1763->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializer57872 && ($this->initializer57872->__invoke($valueHolderf1763, $this, 'getConfiguration', array(), $this->initializer57872) || 1) && $this->valueHolderf1763 = $valueHolderf1763;

        return $this->valueHolderf1763->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializer57872 && ($this->initializer57872->__invoke($valueHolderf1763, $this, 'isOpen', array(), $this->initializer57872) || 1) && $this->valueHolderf1763 = $valueHolderf1763;

        return $this->valueHolderf1763->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializer57872 && ($this->initializer57872->__invoke($valueHolderf1763, $this, 'getUnitOfWork', array(), $this->initializer57872) || 1) && $this->valueHolderf1763 = $valueHolderf1763;

        return $this->valueHolderf1763->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializer57872 && ($this->initializer57872->__invoke($valueHolderf1763, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer57872) || 1) && $this->valueHolderf1763 = $valueHolderf1763;

        return $this->valueHolderf1763->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializer57872 && ($this->initializer57872->__invoke($valueHolderf1763, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer57872) || 1) && $this->valueHolderf1763 = $valueHolderf1763;

        return $this->valueHolderf1763->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializer57872 && ($this->initializer57872->__invoke($valueHolderf1763, $this, 'getProxyFactory', array(), $this->initializer57872) || 1) && $this->valueHolderf1763 = $valueHolderf1763;

        return $this->valueHolderf1763->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializer57872 && ($this->initializer57872->__invoke($valueHolderf1763, $this, 'initializeObject', array('obj' => $obj), $this->initializer57872) || 1) && $this->valueHolderf1763 = $valueHolderf1763;

        return $this->valueHolderf1763->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializer57872 && ($this->initializer57872->__invoke($valueHolderf1763, $this, 'getFilters', array(), $this->initializer57872) || 1) && $this->valueHolderf1763 = $valueHolderf1763;

        return $this->valueHolderf1763->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializer57872 && ($this->initializer57872->__invoke($valueHolderf1763, $this, 'isFiltersStateClean', array(), $this->initializer57872) || 1) && $this->valueHolderf1763 = $valueHolderf1763;

        return $this->valueHolderf1763->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializer57872 && ($this->initializer57872->__invoke($valueHolderf1763, $this, 'hasFilters', array(), $this->initializer57872) || 1) && $this->valueHolderf1763 = $valueHolderf1763;

        return $this->valueHolderf1763->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializer57872 = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolderf1763) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolderf1763 = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolderf1763->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializer57872 && ($this->initializer57872->__invoke($valueHolderf1763, $this, '__get', ['name' => $name], $this->initializer57872) || 1) && $this->valueHolderf1763 = $valueHolderf1763;

        if (isset(self::$publicProperties9826d[$name])) {
            return $this->valueHolderf1763->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderf1763;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolderf1763;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer57872 && ($this->initializer57872->__invoke($valueHolderf1763, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer57872) || 1) && $this->valueHolderf1763 = $valueHolderf1763;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderf1763;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolderf1763;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer57872 && ($this->initializer57872->__invoke($valueHolderf1763, $this, '__isset', array('name' => $name), $this->initializer57872) || 1) && $this->valueHolderf1763 = $valueHolderf1763;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderf1763;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolderf1763;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer57872 && ($this->initializer57872->__invoke($valueHolderf1763, $this, '__unset', array('name' => $name), $this->initializer57872) || 1) && $this->valueHolderf1763 = $valueHolderf1763;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolderf1763;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolderf1763;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializer57872 && ($this->initializer57872->__invoke($valueHolderf1763, $this, '__clone', array(), $this->initializer57872) || 1) && $this->valueHolderf1763 = $valueHolderf1763;

        $this->valueHolderf1763 = clone $this->valueHolderf1763;
    }

    public function __sleep()
    {
        $this->initializer57872 && ($this->initializer57872->__invoke($valueHolderf1763, $this, '__sleep', array(), $this->initializer57872) || 1) && $this->valueHolderf1763 = $valueHolderf1763;

        return array('valueHolderf1763');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer57872 = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer57872;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer57872 && ($this->initializer57872->__invoke($valueHolderf1763, $this, 'initializeProxy', array(), $this->initializer57872) || 1) && $this->valueHolderf1763 = $valueHolderf1763;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolderf1763;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolderf1763;
    }


}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
