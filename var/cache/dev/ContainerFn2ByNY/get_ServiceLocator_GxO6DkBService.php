<?php

namespace ContainerFn2ByNY;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class get_ServiceLocator_GxO6DkBService extends App_KernelDevDebugContainer
{
    /**
     * Gets the private '.service_locator.gxO6DkB' shared service.
     *
     * @return \Symfony\Component\DependencyInjection\ServiceLocator
     */
    public static function do($container, $lazyLoad = true)
    {
        return $container->privates['.service_locator.gxO6DkB'] = new \Symfony\Component\DependencyInjection\Argument\ServiceLocator($container->getService, [
            'trouver' => ['privates', '.errored..service_locator.gxO6DkB.App\\Entity\\Trouver', NULL, 'Cannot autowire service ".service_locator.gxO6DkB": it references class "App\\Entity\\Trouver" but no such service exists.'],
        ], [
            'trouver' => 'App\\Entity\\Trouver',
        ]);
    }
}
