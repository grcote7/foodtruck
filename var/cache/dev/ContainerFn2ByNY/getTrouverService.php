<?php

namespace ContainerFn2ByNY;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class getTrouverService extends App_KernelDevDebugContainer
{
    /**
     * Gets the private '.errored..service_locator.gxO6DkB.App\Entity\Trouver' shared service.
     *
     * @return \App\Entity\Trouver
     */
    public static function do($container, $lazyLoad = true)
    {
        $container->throw('Cannot autowire service ".service_locator.gxO6DkB": it references class "App\\Entity\\Trouver" but no such service exists.');
    }
}
