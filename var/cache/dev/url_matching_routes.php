<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/_profiler' => [[['_route' => '_profiler_home', '_controller' => 'web_profiler.controller.profiler::homeAction'], null, null, null, true, false, null]],
        '/_profiler/search' => [[['_route' => '_profiler_search', '_controller' => 'web_profiler.controller.profiler::searchAction'], null, null, null, false, false, null]],
        '/_profiler/search_bar' => [[['_route' => '_profiler_search_bar', '_controller' => 'web_profiler.controller.profiler::searchBarAction'], null, null, null, false, false, null]],
        '/_profiler/phpinfo' => [[['_route' => '_profiler_phpinfo', '_controller' => 'web_profiler.controller.profiler::phpinfoAction'], null, null, null, false, false, null]],
        '/_profiler/open' => [[['_route' => '_profiler_open_file', '_controller' => 'web_profiler.controller.profiler::openAction'], null, null, null, false, false, null]],
        '/admin/categorie' => [[['_route' => 'admin_categorie', '_controller' => 'App\\Controller\\Admin\\CategorieController::viewAll'], null, null, null, false, false, null]],
        '/admin/categorie/add' => [[['_route' => 'admin_categorie_add', '_controller' => 'App\\Controller\\Admin\\CategorieController::add'], null, null, null, false, false, null]],
        '/admin/produit/categorie' => [[['_route' => 'admin_produit_categorie', '_controller' => 'App\\Controller\\Admin\\CategorieController::Adminproduct'], null, null, null, false, false, null]],
        '/admin/cpanel' => [[['_route' => 'admin_cpanel', '_controller' => 'App\\Controller\\Admin\\CpanelController::index'], null, null, null, false, false, null]],
        '/admin/produit' => [[['_route' => 'admin_produit', '_controller' => 'App\\Controller\\Admin\\ProduitController::viewAll'], null, null, null, false, false, null]],
        '/admin/produit/add' => [[['_route' => 'admin_produit_add', '_controller' => 'App\\Controller\\Admin\\ProduitController::addProductWithImage'], null, null, null, false, false, null]],
        '/front/main/quisommesnous' => [[['_route' => 'front_main_qui_sommes_nous', '_controller' => 'App\\Controller\\Admin\\QuisommesnousController::quiSommesNous'], null, null, null, false, false, null]],
        '/admin/quisommesnous' => [[['_route' => 'admin_quisommesnous', '_controller' => 'App\\Controller\\Admin\\QuisommesnousController::viewAll'], null, null, null, false, false, null]],
        '/admin/quisommesnous/add' => [[['_route' => 'admin_quisommesnous_add', '_controller' => 'App\\Controller\\Admin\\QuisommesnousController::addQuiSommesnousWithImage'], null, null, null, false, false, null]],
        '/admin/trouver' => [[['_route' => 'admin_trouver', '_controller' => 'App\\Controller\\Admin\\TrouverController::trouverAll'], null, null, null, false, false, null]],
        '/admin/trouver/add' => [[['_route' => 'admin_trouver_add', '_controller' => 'App\\Controller\\Admin\\TrouverController::addWithImage'], null, null, null, false, false, null]],
        '/front/main/categorie' => [[['_route' => 'front_main_categorie', '_controller' => 'App\\Controller\\Admin\\TrouverController::test'], null, null, null, false, false, null]],
        '/admin/users' => [[['_route' => 'admin_users', '_controller' => 'App\\Controller\\Admin\\UsersController::users'], null, null, null, false, false, null]],
        '/front/main' => [[['_route' => 'front_main', '_controller' => 'App\\Controller\\Front\\MainController::index'], null, null, null, false, false, null]],
        '/front/main/produit' => [[['_route' => 'front_main_produit', '_controller' => 'App\\Controller\\Front\\MainController::product'], null, null, null, false, false, null]],
        '/front/main/contact' => [[['_route' => 'front_main_contact', '_controller' => 'App\\Controller\\Front\\MainController::contact'], null, null, null, false, false, null]],
        '/front/main/trouver' => [[['_route' => 'front_main_trouver', '_controller' => 'App\\Controller\\Front\\MainController::trouverAllS'], null, null, null, false, false, null]],
        '/login' => [[['_route' => 'app_login', '_controller' => 'App\\Controller\\SecurityController::login'], null, null, null, false, false, null]],
        '/logout' => [[['_route' => 'app_logout', '_controller' => 'App\\Controller\\SecurityController::logout'], null, null, null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/_(?'
                    .'|error/(\\d+)(?:\\.([^/]++))?(*:38)'
                    .'|wdt/([^/]++)(*:57)'
                    .'|profiler/([^/]++)(?'
                        .'|/(?'
                            .'|search/results(*:102)'
                            .'|router(*:116)'
                            .'|exception(?'
                                .'|(*:136)'
                                .'|\\.css(*:149)'
                            .')'
                        .')'
                        .'|(*:159)'
                    .')'
                .')'
                .'|/admin/(?'
                    .'|categorie/(?'
                        .'|edit/(\\d+)(*:202)'
                        .'|de(?'
                            .'|tail/(\\d+)(*:225)'
                            .'|lete/([^/]++)(*:246)'
                        .')'
                    .')'
                    .'|produit/(?'
                        .'|edit/(\\d+)(*:277)'
                        .'|delete/([^/]++)(*:300)'
                    .')'
                    .'|quisommesnous/(?'
                        .'|edit/(\\d+)(*:336)'
                        .'|delete/([^/]++)(*:359)'
                    .')'
                    .'|trouver/(?'
                        .'|edit/(\\d+)(*:389)'
                        .'|delete/([^/]++)(*:412)'
                    .')'
                    .'|users/edit/(\\d+)(*:437)'
                .')'
                .'|/front/main/categ/(\\d+)(*:469)'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        38 => [[['_route' => '_preview_error', '_controller' => 'error_controller::preview', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
        57 => [[['_route' => '_wdt', '_controller' => 'web_profiler.controller.profiler::toolbarAction'], ['token'], null, null, false, true, null]],
        102 => [[['_route' => '_profiler_search_results', '_controller' => 'web_profiler.controller.profiler::searchResultsAction'], ['token'], null, null, false, false, null]],
        116 => [[['_route' => '_profiler_router', '_controller' => 'web_profiler.controller.router::panelAction'], ['token'], null, null, false, false, null]],
        136 => [[['_route' => '_profiler_exception', '_controller' => 'web_profiler.controller.exception_panel::body'], ['token'], null, null, false, false, null]],
        149 => [[['_route' => '_profiler_exception_css', '_controller' => 'web_profiler.controller.exception_panel::stylesheet'], ['token'], null, null, false, false, null]],
        159 => [[['_route' => '_profiler', '_controller' => 'web_profiler.controller.profiler::panelAction'], ['token'], null, null, false, true, null]],
        202 => [[['_route' => 'admin_categorie_edit', '_controller' => 'App\\Controller\\Admin\\CategorieController::editCategorie'], ['id'], null, null, false, true, null]],
        225 => [[['_route' => 'admin_categorie_detail', '_controller' => 'App\\Controller\\Admin\\CategorieController::adminCategorieDetail'], ['id'], null, null, false, true, null]],
        246 => [[['_route' => 'admin_categorie_delete', '_controller' => 'App\\Controller\\Admin\\CategorieController::deleteCategorie'], ['id'], null, null, false, true, null]],
        277 => [[['_route' => 'admin_produit_edit', '_controller' => 'App\\Controller\\Admin\\ProduitController::editProduct'], ['id'], null, null, false, true, null]],
        300 => [[['_route' => 'admin_produit_delete', '_controller' => 'App\\Controller\\Admin\\ProduitController::deleteProduct'], ['id'], null, null, false, true, null]],
        336 => [[['_route' => 'admin_quisommesnous_edit', '_controller' => 'App\\Controller\\Admin\\QuisommesnousController::editProduct'], ['id'], null, null, false, true, null]],
        359 => [[['_route' => 'admin_quisommesnous_delete', '_controller' => 'App\\Controller\\Admin\\QuisommesnousController::deleteProduct'], ['id'], null, null, false, true, null]],
        389 => [[['_route' => 'admin_trouver_edit', '_controller' => 'App\\Controller\\Admin\\TrouverController::editLoc'], ['id'], null, null, false, true, null]],
        412 => [[['_route' => 'admin_trouver_delete', '_controller' => 'App\\Controller\\Admin\\TrouverController::delete'], ['id'], null, null, false, true, null]],
        437 => [[['_route' => 'admin_users_edit', '_controller' => 'App\\Controller\\Admin\\UsersController::editUsers'], ['id'], null, null, false, true, null]],
        469 => [
            [['_route' => 'front_main_categ', '_controller' => 'App\\Controller\\Admin\\CategorieController::categorieDetail'], ['id'], null, null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
