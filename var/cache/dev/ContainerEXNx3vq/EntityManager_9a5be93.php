<?php

namespace ContainerEXNx3vq;
include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/lib/Doctrine/Persistence/ObjectManager.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{

    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolder8927b = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer79502 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties1bab9 = [
        
    ];

    public function getConnection()
    {
        $this->initializer79502 && ($this->initializer79502->__invoke($valueHolder8927b, $this, 'getConnection', array(), $this->initializer79502) || 1) && $this->valueHolder8927b = $valueHolder8927b;

        return $this->valueHolder8927b->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializer79502 && ($this->initializer79502->__invoke($valueHolder8927b, $this, 'getMetadataFactory', array(), $this->initializer79502) || 1) && $this->valueHolder8927b = $valueHolder8927b;

        return $this->valueHolder8927b->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializer79502 && ($this->initializer79502->__invoke($valueHolder8927b, $this, 'getExpressionBuilder', array(), $this->initializer79502) || 1) && $this->valueHolder8927b = $valueHolder8927b;

        return $this->valueHolder8927b->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializer79502 && ($this->initializer79502->__invoke($valueHolder8927b, $this, 'beginTransaction', array(), $this->initializer79502) || 1) && $this->valueHolder8927b = $valueHolder8927b;

        return $this->valueHolder8927b->beginTransaction();
    }

    public function getCache()
    {
        $this->initializer79502 && ($this->initializer79502->__invoke($valueHolder8927b, $this, 'getCache', array(), $this->initializer79502) || 1) && $this->valueHolder8927b = $valueHolder8927b;

        return $this->valueHolder8927b->getCache();
    }

    public function transactional($func)
    {
        $this->initializer79502 && ($this->initializer79502->__invoke($valueHolder8927b, $this, 'transactional', array('func' => $func), $this->initializer79502) || 1) && $this->valueHolder8927b = $valueHolder8927b;

        return $this->valueHolder8927b->transactional($func);
    }

    public function commit()
    {
        $this->initializer79502 && ($this->initializer79502->__invoke($valueHolder8927b, $this, 'commit', array(), $this->initializer79502) || 1) && $this->valueHolder8927b = $valueHolder8927b;

        return $this->valueHolder8927b->commit();
    }

    public function rollback()
    {
        $this->initializer79502 && ($this->initializer79502->__invoke($valueHolder8927b, $this, 'rollback', array(), $this->initializer79502) || 1) && $this->valueHolder8927b = $valueHolder8927b;

        return $this->valueHolder8927b->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializer79502 && ($this->initializer79502->__invoke($valueHolder8927b, $this, 'getClassMetadata', array('className' => $className), $this->initializer79502) || 1) && $this->valueHolder8927b = $valueHolder8927b;

        return $this->valueHolder8927b->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializer79502 && ($this->initializer79502->__invoke($valueHolder8927b, $this, 'createQuery', array('dql' => $dql), $this->initializer79502) || 1) && $this->valueHolder8927b = $valueHolder8927b;

        return $this->valueHolder8927b->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializer79502 && ($this->initializer79502->__invoke($valueHolder8927b, $this, 'createNamedQuery', array('name' => $name), $this->initializer79502) || 1) && $this->valueHolder8927b = $valueHolder8927b;

        return $this->valueHolder8927b->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer79502 && ($this->initializer79502->__invoke($valueHolder8927b, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer79502) || 1) && $this->valueHolder8927b = $valueHolder8927b;

        return $this->valueHolder8927b->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializer79502 && ($this->initializer79502->__invoke($valueHolder8927b, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer79502) || 1) && $this->valueHolder8927b = $valueHolder8927b;

        return $this->valueHolder8927b->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializer79502 && ($this->initializer79502->__invoke($valueHolder8927b, $this, 'createQueryBuilder', array(), $this->initializer79502) || 1) && $this->valueHolder8927b = $valueHolder8927b;

        return $this->valueHolder8927b->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializer79502 && ($this->initializer79502->__invoke($valueHolder8927b, $this, 'flush', array('entity' => $entity), $this->initializer79502) || 1) && $this->valueHolder8927b = $valueHolder8927b;

        return $this->valueHolder8927b->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer79502 && ($this->initializer79502->__invoke($valueHolder8927b, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer79502) || 1) && $this->valueHolder8927b = $valueHolder8927b;

        return $this->valueHolder8927b->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializer79502 && ($this->initializer79502->__invoke($valueHolder8927b, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer79502) || 1) && $this->valueHolder8927b = $valueHolder8927b;

        return $this->valueHolder8927b->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer79502 && ($this->initializer79502->__invoke($valueHolder8927b, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer79502) || 1) && $this->valueHolder8927b = $valueHolder8927b;

        return $this->valueHolder8927b->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializer79502 && ($this->initializer79502->__invoke($valueHolder8927b, $this, 'clear', array('entityName' => $entityName), $this->initializer79502) || 1) && $this->valueHolder8927b = $valueHolder8927b;

        return $this->valueHolder8927b->clear($entityName);
    }

    public function close()
    {
        $this->initializer79502 && ($this->initializer79502->__invoke($valueHolder8927b, $this, 'close', array(), $this->initializer79502) || 1) && $this->valueHolder8927b = $valueHolder8927b;

        return $this->valueHolder8927b->close();
    }

    public function persist($entity)
    {
        $this->initializer79502 && ($this->initializer79502->__invoke($valueHolder8927b, $this, 'persist', array('entity' => $entity), $this->initializer79502) || 1) && $this->valueHolder8927b = $valueHolder8927b;

        return $this->valueHolder8927b->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializer79502 && ($this->initializer79502->__invoke($valueHolder8927b, $this, 'remove', array('entity' => $entity), $this->initializer79502) || 1) && $this->valueHolder8927b = $valueHolder8927b;

        return $this->valueHolder8927b->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializer79502 && ($this->initializer79502->__invoke($valueHolder8927b, $this, 'refresh', array('entity' => $entity), $this->initializer79502) || 1) && $this->valueHolder8927b = $valueHolder8927b;

        return $this->valueHolder8927b->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializer79502 && ($this->initializer79502->__invoke($valueHolder8927b, $this, 'detach', array('entity' => $entity), $this->initializer79502) || 1) && $this->valueHolder8927b = $valueHolder8927b;

        return $this->valueHolder8927b->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializer79502 && ($this->initializer79502->__invoke($valueHolder8927b, $this, 'merge', array('entity' => $entity), $this->initializer79502) || 1) && $this->valueHolder8927b = $valueHolder8927b;

        return $this->valueHolder8927b->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializer79502 && ($this->initializer79502->__invoke($valueHolder8927b, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer79502) || 1) && $this->valueHolder8927b = $valueHolder8927b;

        return $this->valueHolder8927b->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer79502 && ($this->initializer79502->__invoke($valueHolder8927b, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer79502) || 1) && $this->valueHolder8927b = $valueHolder8927b;

        return $this->valueHolder8927b->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializer79502 && ($this->initializer79502->__invoke($valueHolder8927b, $this, 'getRepository', array('entityName' => $entityName), $this->initializer79502) || 1) && $this->valueHolder8927b = $valueHolder8927b;

        return $this->valueHolder8927b->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializer79502 && ($this->initializer79502->__invoke($valueHolder8927b, $this, 'contains', array('entity' => $entity), $this->initializer79502) || 1) && $this->valueHolder8927b = $valueHolder8927b;

        return $this->valueHolder8927b->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializer79502 && ($this->initializer79502->__invoke($valueHolder8927b, $this, 'getEventManager', array(), $this->initializer79502) || 1) && $this->valueHolder8927b = $valueHolder8927b;

        return $this->valueHolder8927b->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializer79502 && ($this->initializer79502->__invoke($valueHolder8927b, $this, 'getConfiguration', array(), $this->initializer79502) || 1) && $this->valueHolder8927b = $valueHolder8927b;

        return $this->valueHolder8927b->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializer79502 && ($this->initializer79502->__invoke($valueHolder8927b, $this, 'isOpen', array(), $this->initializer79502) || 1) && $this->valueHolder8927b = $valueHolder8927b;

        return $this->valueHolder8927b->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializer79502 && ($this->initializer79502->__invoke($valueHolder8927b, $this, 'getUnitOfWork', array(), $this->initializer79502) || 1) && $this->valueHolder8927b = $valueHolder8927b;

        return $this->valueHolder8927b->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializer79502 && ($this->initializer79502->__invoke($valueHolder8927b, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer79502) || 1) && $this->valueHolder8927b = $valueHolder8927b;

        return $this->valueHolder8927b->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializer79502 && ($this->initializer79502->__invoke($valueHolder8927b, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer79502) || 1) && $this->valueHolder8927b = $valueHolder8927b;

        return $this->valueHolder8927b->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializer79502 && ($this->initializer79502->__invoke($valueHolder8927b, $this, 'getProxyFactory', array(), $this->initializer79502) || 1) && $this->valueHolder8927b = $valueHolder8927b;

        return $this->valueHolder8927b->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializer79502 && ($this->initializer79502->__invoke($valueHolder8927b, $this, 'initializeObject', array('obj' => $obj), $this->initializer79502) || 1) && $this->valueHolder8927b = $valueHolder8927b;

        return $this->valueHolder8927b->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializer79502 && ($this->initializer79502->__invoke($valueHolder8927b, $this, 'getFilters', array(), $this->initializer79502) || 1) && $this->valueHolder8927b = $valueHolder8927b;

        return $this->valueHolder8927b->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializer79502 && ($this->initializer79502->__invoke($valueHolder8927b, $this, 'isFiltersStateClean', array(), $this->initializer79502) || 1) && $this->valueHolder8927b = $valueHolder8927b;

        return $this->valueHolder8927b->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializer79502 && ($this->initializer79502->__invoke($valueHolder8927b, $this, 'hasFilters', array(), $this->initializer79502) || 1) && $this->valueHolder8927b = $valueHolder8927b;

        return $this->valueHolder8927b->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializer79502 = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolder8927b) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder8927b = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolder8927b->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializer79502 && ($this->initializer79502->__invoke($valueHolder8927b, $this, '__get', ['name' => $name], $this->initializer79502) || 1) && $this->valueHolder8927b = $valueHolder8927b;

        if (isset(self::$publicProperties1bab9[$name])) {
            return $this->valueHolder8927b->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder8927b;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder8927b;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializer79502 && ($this->initializer79502->__invoke($valueHolder8927b, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer79502) || 1) && $this->valueHolder8927b = $valueHolder8927b;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder8927b;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder8927b;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializer79502 && ($this->initializer79502->__invoke($valueHolder8927b, $this, '__isset', array('name' => $name), $this->initializer79502) || 1) && $this->valueHolder8927b = $valueHolder8927b;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder8927b;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolder8927b;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializer79502 && ($this->initializer79502->__invoke($valueHolder8927b, $this, '__unset', array('name' => $name), $this->initializer79502) || 1) && $this->valueHolder8927b = $valueHolder8927b;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder8927b;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolder8927b;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializer79502 && ($this->initializer79502->__invoke($valueHolder8927b, $this, '__clone', array(), $this->initializer79502) || 1) && $this->valueHolder8927b = $valueHolder8927b;

        $this->valueHolder8927b = clone $this->valueHolder8927b;
    }

    public function __sleep()
    {
        $this->initializer79502 && ($this->initializer79502->__invoke($valueHolder8927b, $this, '__sleep', array(), $this->initializer79502) || 1) && $this->valueHolder8927b = $valueHolder8927b;

        return array('valueHolder8927b');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer79502 = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer79502;
    }

    public function initializeProxy() : bool
    {
        return $this->initializer79502 && ($this->initializer79502->__invoke($valueHolder8927b, $this, 'initializeProxy', array(), $this->initializer79502) || 1) && $this->valueHolder8927b = $valueHolder8927b;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder8927b;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder8927b;
    }


}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
