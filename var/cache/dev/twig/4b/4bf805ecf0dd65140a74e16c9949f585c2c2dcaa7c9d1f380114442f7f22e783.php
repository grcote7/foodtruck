<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/cpanel/index.html.twig */
class __TwigTemplate_142415604906146041f2e64c77aef2d4b3eb61f712284f84023d6832feb69e9e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/cpanel/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/cpanel/index.html.twig"));

        $this->parent = $this->loadTemplate("admin.html.twig", "admin/cpanel/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Interface d'administration générale!";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "<div class=\"container\">
<div class=\"container\">
    <div class=\"my-5 row\">
        <div class=\"card border-primary mb-3 mx-2\" style=\"max-width: 20rem;\">
            <div class=\"card-header text-center\">
                <h2><a class=\"cpanel\" href=\"";
        // line 11
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_categorie");
        echo "\">Catégorie(s)</a></h2>
            </div>
            <div class=\"card-body\">
                <h4 class=\"card-title1\">Toute les catégories</h4>
                <p class=\"card-text\">Ajouter , modifier ou supprimer les catgeories.</p>
            </div>
        </div>
        <div class=\"card border-dark mb-3 mx-2\" style=\"max-width: 20rem;\">
            <div class=\"card-header text-center\">
                <h2><a class=\"cpanel\" href=\"";
        // line 20
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_produit_categorie");
        echo "\">Produit(s)</a></h2>
            </div>
            <div class=\"card-body\">
                <h4 class=\"card-title\">Tous les produits</h4>
                <p class=\"card-text\">Ajouter , modifier ou supprimer des produits.</p>
            </div>
        </div>
        <div class=\"card border-success mb-3 mx-2\" style=\"max-width: 20rem;\">
            <div class=\"card-header text-center\">
                <h2><a class=\"cpanel\" href=\"";
        // line 29
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_trouver");
        echo "\">Où nous trouver</a></h2>
            </div>
            <div class=\"card-body\">
                <h4 class=\"card-title\">Localisation</h4>
                <p class=\"card-text\">Les differents endroits ou nous retrouver avec nos horaires.</p>
            </div>
        </div>
        <div class=\"card border-info mb-3 mx-2\" style=\"max-width: 20rem;\">
            <div class=\"card-header text-center\">
                <h2>A venir(s)</h2>
            </div>
            <div class=\"card-body\">
                <h4 class=\"card-title\">A venir</h4>
                <p class=\"card-text\">Dans le but d'une futur option a developper</p>
            </div>
        </div>
        <div class=\"card border-warning mb-3 mx-2\" style=\"max-width: 20rem;\">
            <div class=\"card-header text-center\">
                <h2>A venir(s)</h2>
            </div>
            <div class=\"card-body\">
                <h4 class=\"card-title\">A venir</h4>
                <p class=\"card-text\">Dans le but d'une futur option a developper</p>
            </div>
        </div>
        <div class=\"card border-danger mb-3 mx-2\" style=\"max-width: 20rem;\">
            <div class=\"card-header text-center\">
                <h2><a class=\"cpanel\" href=\"";
        // line 56
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_users");
        echo "\">Information</a></h2>
            </div>
            <div class=\"card-body\">
                <h4 class=\"card-title\">Détail du compte</h4>
                <p class=\"card-text\">Modifier mes informations de connexion.</p>
            </div>
        </div>
    </div>
</div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "admin/cpanel/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  149 => 56,  119 => 29,  107 => 20,  95 => 11,  88 => 6,  78 => 5,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'admin.html.twig' %}

{% block title %}Interface d'administration générale!{% endblock %}

{% block body %}
<div class=\"container\">
<div class=\"container\">
    <div class=\"my-5 row\">
        <div class=\"card border-primary mb-3 mx-2\" style=\"max-width: 20rem;\">
            <div class=\"card-header text-center\">
                <h2><a class=\"cpanel\" href=\"{{ path('admin_categorie') }}\">Catégorie(s)</a></h2>
            </div>
            <div class=\"card-body\">
                <h4 class=\"card-title1\">Toute les catégories</h4>
                <p class=\"card-text\">Ajouter , modifier ou supprimer les catgeories.</p>
            </div>
        </div>
        <div class=\"card border-dark mb-3 mx-2\" style=\"max-width: 20rem;\">
            <div class=\"card-header text-center\">
                <h2><a class=\"cpanel\" href=\"{{ path('admin_produit_categorie') }}\">Produit(s)</a></h2>
            </div>
            <div class=\"card-body\">
                <h4 class=\"card-title\">Tous les produits</h4>
                <p class=\"card-text\">Ajouter , modifier ou supprimer des produits.</p>
            </div>
        </div>
        <div class=\"card border-success mb-3 mx-2\" style=\"max-width: 20rem;\">
            <div class=\"card-header text-center\">
                <h2><a class=\"cpanel\" href=\"{{path('admin_trouver')}}\">Où nous trouver</a></h2>
            </div>
            <div class=\"card-body\">
                <h4 class=\"card-title\">Localisation</h4>
                <p class=\"card-text\">Les differents endroits ou nous retrouver avec nos horaires.</p>
            </div>
        </div>
        <div class=\"card border-info mb-3 mx-2\" style=\"max-width: 20rem;\">
            <div class=\"card-header text-center\">
                <h2>A venir(s)</h2>
            </div>
            <div class=\"card-body\">
                <h4 class=\"card-title\">A venir</h4>
                <p class=\"card-text\">Dans le but d'une futur option a developper</p>
            </div>
        </div>
        <div class=\"card border-warning mb-3 mx-2\" style=\"max-width: 20rem;\">
            <div class=\"card-header text-center\">
                <h2>A venir(s)</h2>
            </div>
            <div class=\"card-body\">
                <h4 class=\"card-title\">A venir</h4>
                <p class=\"card-text\">Dans le but d'une futur option a developper</p>
            </div>
        </div>
        <div class=\"card border-danger mb-3 mx-2\" style=\"max-width: 20rem;\">
            <div class=\"card-header text-center\">
                <h2><a class=\"cpanel\" href=\"{{ path('admin_users') }}\">Information</a></h2>
            </div>
            <div class=\"card-body\">
                <h4 class=\"card-title\">Détail du compte</h4>
                <p class=\"card-text\">Modifier mes informations de connexion.</p>
            </div>
        </div>
    </div>
</div>
</div>
{% endblock %}", "admin/cpanel/index.html.twig", "/var/www/html/foodtruck/templates/admin/cpanel/index.html.twig");
    }
}
